# frozen_string_literal: true

task ci_test: :environment do
  Rake::Task['db:create'].invoke
  Rake::Task['db:migrate'].invoke
  Rake::Task['test'].invoke
end
