#!/bin/bash

# Replace '/path/to/your/rails/app' with the actual path to your Rails app directory
RAILS_APP_DIR="./"
MYSQL_COMMANDS="
Use mysql;
select user from user;
Use performance_schema;
show performance_schema.processlist;
"

# Check if the Rails app directory exists
if [ -d "$RAILS_APP_DIR" ]; then
  # Change to the Rails app directory
  cd "$RAILS_APP_DIR"


  # Open the Rails console run specific commands and exit
    rails dbconsole < <(echo -e "$MYSQL_COMMANDS")
else
  echo "Rails app directory not found at: $RAILS_APP_DIR"
fi